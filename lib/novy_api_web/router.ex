defmodule NovyAPIWeb.Router do
  use NovyAPIWeb, :router
  use Phoenix.Router

  pipeline :api do
    plug :accepts, ["json"]
  end
  
  scope "/", NovyAPIWeb do
    pipe_through :api

    get "/", DefaultController, :index
  end

  forward "/playground", Absinthe.Plug.GraphiQL,
    schema: NovyAPIWeb.Schema,
    # socket: NovyAPIWeb.UserSocket,
    interface: :playground
    # interface: :advanced

  forward "/graphql", Absinthe.Plug,
    schema: NovyAPIWeb.Schema
end
