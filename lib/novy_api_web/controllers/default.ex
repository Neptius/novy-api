defmodule NovyAPIWeb.DefaultController do
    use NovyAPIWeb, :controller
  
    def index(conn, _params) do
        send_resp(conn, 200, "ok")
    end
  end