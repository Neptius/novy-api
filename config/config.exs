# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :novy_api,
  ecto_repos: [NovyAPI.Repo]

# Configures the endpoint
config :novy_api, NovyAPIWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "SzAXJcI604/Ae8A3vrJWdhYCnqAHfcDivnrUmX3Aesf97bc4Nammy+UTGQEQ0BBh",
  render_errors: [view: NovyAPIWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: NovyAPI.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
